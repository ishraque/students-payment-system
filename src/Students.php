<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 7/16/2019
 * Time: 12:02 PM
 */

namespace App;


class Students extends Database
{
    public $userID;
    public $name;
    public $classID;
    public $groupName;
    public $yearID;
    public $sectionID;
    public $created_at;
    public $modified_at;

    public function insertData($name,$classID,$groupName, $yearID, $sectionID, $created_at){

        $sql = "INSERT INTO students SET name=:name,classID=:classID,groupName=:groupName, yearID=:yearID, sectionID=:sectionID, created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':name'=>$name,':classID'=>$classID,
            ':groupName'=>$groupName, ':yearID'=>$yearID, ':sectionID'=>$sectionID, ':created_at'=>$created_at));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id,$name,$classID,$groupName, $yearID, $sectionID, $updated_at){

        $sql = "UPDATE students
 SET name=:name,classID=:classID,groupName=:groupName, yearID=:yearID, sectionID=:sectionID, updated_at=:updated_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id, ':name'=>$name,':classID'=>$classID,
            ':groupName'=>$groupName, ':yearID'=>$yearID, ':sectionID'=>$sectionID, ':updated_at'=>$updated_at ));
        return true;

    }
}