<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 7/16/2019
 * Time: 2:30 PM
 */

namespace App;


class Invoice extends Database
{
    public $id;
    public $invoiceID;
    public $userID;
    public $catID;
    public $amount;
    public $description;
    public $created_at;
    public $modified_at;

    public function insertData($invoiceID, $userID, $catID, $description, $amount, $created_at){

        $sql = "INSERT INTO invoice SET
                        invoiceID=:invoiceID,
                        userID=:userID,
                        amount=:amount,
                        description=:description,
                        catID=:catID,
                        created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(':invoiceID'=>$invoiceID, ':description'=>$description, ':userID'=>$userID,':catID'=>$catID, ':amount'=>$amount, ':created_at'=>$created_at));
        if($r)
            return true;
        else
            return false;
    }

    public function update($id, $invoiceID, $userID, $catID, $amount, $description, $modified_at){

        $sql = "UPDATE invoice SET
                   invoiceID=:invoiceID,
                   userID=:userID,
                   catID=:catID,
                   amount=:amount,
                   description=:description,
                   modified_at=:modified_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $r =  $q->execute(array(':invoiceID'=>$invoiceID, ':description'=>$description, ':amount'=>$amount, ':id'=>$id, ':userID'=>$userID,':catID'=>$catID, ':modified_at'=>$modified_at ));
        if($r)
            return true;
        else
            return false;

    }
}