<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 7/16/2019
 * Time: 12:13 PM
 */

namespace App;


class Category extends Database
{
    public $id;
    public $cat_name;
    public $parent;
    public $created_at;
    public $modified_at;

    public function insertData($cat_name, $parent, $created_at){

        $sql = "INSERT INTO category SET cat_name=:cat_name,parent=:parent, created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(':cat_name'=>$cat_name,':parent'=>$parent, ':created_at'=>$created_at));
        if($r)
            return true;
        else
            return false;
    }

    public function update($id,$cat_name, $parent, $updated_at){

        $sql = "UPDATE category SET cat_name=:cat_name,parent=:parent, modified_at=:modified_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
       $r =  $q->execute(array(':id'=>$id, ':cat_name'=>$cat_name,':parent'=>$parent, ':modified_at'=>$modified_at ));
       if($r)
       return true;
       else
           return false;

    }
}