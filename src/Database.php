<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 7/4/2019
 * Time: 2:47 PM
 */

namespace App;

use PDO;


class Database
{


    private $host="localhost";
    private $user="root";
    private $db="payment";
    private $pass="";
    public $conn;

    public function __construct(){

        $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->db,$this->user,$this->pass);
    }

    public function showData($table){

        $sql="SELECT * FROM $table";
        $q = $this->conn->query($sql) or die("failed!");

        while($r = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]=$r;
        }
        return $data;
    }

    public function getById($id,$table){

        $sql="SELECT * FROM $table WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        return $data;
    }



    public function deleteData($id,$table){

        $sql="DELETE FROM $table WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id));
        return true;
    }



}