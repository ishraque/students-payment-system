<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 7/17/2019
 * Time: 4:20 PM
 */

namespace App;


class InvoiceID extends Database
{
   public $id;
   public $userID;
   public $amount;
   public $created_at;
   public $modified_at;

    public function insertData($userID, $amount, $created_at){

        $sql = "INSERT INTO invoiceid SET
                        userID=:userID,
                        amount=:amount,
                        created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(':userID'=>$userID, ':amount'=>$amount, ':created_at'=>$created_at));
        if($r)
            return true;
        else
            return false;
    }

    public function update($id, $userID, $amount, $modified_at){

        $sql = "UPDATE invoiceid SET
                   userID=:userID,
                   amount=:amount,
                   modified_at=:modified_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $r =  $q->execute(array(':amount'=>$amount, ':id'=>$id, ':userID'=>$userID, ':modified_at'=>$modified_at ));
        if($r)
            return true;
        else
            return false;

    }
}