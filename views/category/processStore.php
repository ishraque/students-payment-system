<?php

include("../../vendor/autoload.php");

use App\Category;

$cat = new Category();

if(isset($_POST['submit']))
{
    $cat_name = $_POST['cat_name'];
    $parent = $_POST['parent'];
    $created_at = date("Y-m-d h:i:s", time()) ;
//$table = $_POST['table'];
    $table = "category";
    $r = $cat->insertData($cat_name, $parent, $created_at );

    if($r){
        header("Location:../../index.php");
    }
    else
        echo "Connection error";
}
